const express = require("express");
const router = express.Router();
const productController = require("../controllers/ctrl_product");
const auth = require("../auth");

// Route for creating a product (ADMIN ONLY)
router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.createProduct(req.body, userData)
	.then(result => res.send(result));
});

// Route for retrieving all active products
router.get('/', (req, res) => {
	productController.getAllProducts()
	.then(result => res.send(result));
});

// Route for retrieving a specific product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params)
	.then(result => res.send(result));
});

// Route for Product Updating (ADMIN ONLY)
router.put("/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.updateProduct(req.params.productId, req.body, userData.isAdmin)
	.then(resultFromController => {
		res.send(resultFromController)
	});
});

// Route for Product Archiving (ADMIN ONLY)
router.put('/:productId/archive', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.archiveProduct(userData, req.params)
	.then(result => res.send(result));
});

module.exports = router;