const Order = require('../models/mdl_order');
const Product = require('../models/mdl_product');
const User = require('../models/mdl_user');
const auth = require('../auth');

// Controller for creating an order/checking out (NON-ADMIN ONLY)
module.exports.createOrder = async (userData, reqBody) => {
	if(!userData.isAdmin){
		let userID = await User.findOne({_id: userData.id})
		.then(result => {
			return result.id;
		});

		let productID = await Product.findOne({_id: reqBody.productId})
		.then(result => {
			return result.id;
		});

		let price = await Product.findOne({_id: reqBody.productId})
		.then(result => {
			return result.price;
		});

		let productQuantity = await Product.findOne({_id: reqBody.productId})
		.then(result => {
			return result.stocks;
		});

		let productName = await Product.findOne({_id: reqBody.productId})
		.then(result => {
			return result.name;
		});

		let myOrder = new Order({
			userId: userID,
			productId : productID,
			orderQuantity: reqBody.quantity,
			totalAmount : reqBody.quantity * price
		})

		Product.findOne({_id: reqBody.productId})
		.then(result => {
			if(result.stocks >= reqBody.quantity) {
				let productSummary = {
					orderedOn : myOrder.orderedOn,
					orderId : myOrder.id,
					orderQuantity : reqBody.quantity
				}

				result.stocks = result.stocks - reqBody.quantity;
				result.orderSummary.push(productSummary);

				return result.save()
				.then((result, error) => {
					if(error) {
						return `Updating product failed.`;
					}
					else {
						return `Successfully updated the product.`;
					}
				})
			}
			else {
				return `The amount of ${productName} available is not enough for this order.`;
			}
		})

		User.findOne({_id: userData.id})
		.then(result => {
			if(productQuantity >= myOrder.quantity){
				let orderSummary = {
					orderedOn : myOrder.orderedOn,
					orderId : myOrder.id,
					productId : myOrder.productId,
					quantity : myOrder.quantity,
					totalAmount : myOrder.totalAmount
				}

				result.orders.push(orderSummary);

				return result.save()
				.then((result, error) => {
					if(error) {
						return `User data update has failed.`;
					} else {
						return `Successfully updated the user.`;
					}
				})
			} else {
				return `The amount of ${productName} available is not enough for this order.`;
			}
		})

		if(productQuantity >= reqBody.quantity) {
			return myOrder.save()
			.then((order, error) => {
				if(error) {
					return `The new order request failed.`;
				} else {
					return `You have successfully created your order!`;
				}
			})
		}
		else {
			return `The amount of ${productName} available is not enough for this order.`;
		}

	} else {
		return `ERROR: Feature is for customer-level access only.`;
	}
}

// Controller for getting a user's order/s (NON-ADMIN ONLY)
module.exports.getMyOrders = async (userData) => {
	if(!userData.isAdmin){
		return Order.findOne({userId:userData.id}).then(result => {
			if (result == null){
        return false
      }
        else{
          return result;
        }
      })
    }
    else{
      return false
   }
}

// Controller for getting all orders (ADMIN ONLY)
module.exports.getAllOrders = async (userData) => {
	if(userData.isAdmin){
		return Order.find().then(result => {
			return result;
		})
	} else {
		return `You are not authorized to do this action.`
	}
}



